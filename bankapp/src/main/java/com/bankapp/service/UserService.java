package com.bankapp.service;

import java.util.List;

import javax.validation.Valid;

import com.bankapp.dto.UserAccountDto;
import com.bankapp.dto.UserRequestAccountDetails;
import com.bankapp.dto.UserRequestDetails;
import com.bankapp.model.User;
import com.bankapp.model.UserAccount;

public interface UserService {

	public abstract User saveUser(UserRequestDetails userRequestDetails);

	public abstract List<UserRequestDetails> getAllUsers();

	public abstract boolean deleteUser(int userId);

	public abstract String getUsersByEmail(String email);

	public abstract UserAccount createUserAccount(UserAccountDto userAccountDto);

	public abstract UserAccount getUserAccountNumber(int userId);

	public abstract User updateUser(@Valid User user);

	public abstract UserRequestAccountDetails getUserAccountDetails(int userId);

	public abstract List<UserRequestAccountDetails> getAllAccountDetails();

	public abstract Boolean getUserByPhoneNumber(String phoneNumber);

	public abstract User findUserByPhoneNumber(String fromPhoneNumber);

}
