package com.bankapp.service;

import java.util.List;

import com.bankapp.dto.FundTransferDto;
import com.bankapp.exception.RecordsNotFoundException;
import com.bankapp.model.UserFundTransfer;

public interface UserFundTransferService {

	public abstract UserFundTransfer addFund(FundTransferDto fundTransferDto);

	public abstract List<FundTransferDto> getMonthlyStatements(int  year,int month) throws RecordsNotFoundException;

	public abstract List<FundTransferDto> getHistoryDetails();

	

	

}
