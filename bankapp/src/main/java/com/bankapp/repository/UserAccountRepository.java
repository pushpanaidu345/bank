package com.bankapp.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import com.bankapp.model.UserAccount;

public interface UserAccountRepository extends JpaRepository<UserAccount, Integer> {

	UserAccount findByUser_userId(int userId);

	UserAccount findByAccountNumber(long fromAccountNumber);

}
