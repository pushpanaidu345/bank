package com.bankapp.service;

import java.time.LocalDateTime;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bankapp.dto.FundTransferDto;
import com.bankapp.dto.FundTransferUsingPhoneNumbersDto;
import com.bankapp.exception.ResourceNotFoundException;
import com.bankapp.model.User;
import com.bankapp.model.UserAccount;
import com.bankapp.model.UserFundTransfer;
import com.bankapp.repository.UserAccountRepository;

@Service
public class FundTransferUsingPhoneNumbersServiceImpl implements FundTransferUsingPhoneNumbersService {
	
	@Autowired
	private UserService userService;
	
	@Autowired
	private UserAccountRepository userAccountRepository;
	
	@Autowired
	private UserFundTransferService userFundTransferService;

	@Override
	public String transferAmount(
			
			FundTransferUsingPhoneNumbersDto fundTransferUsingPhoneNumbersDto) {
		
		User fromUser=userService.findUserByPhoneNumber(fundTransferUsingPhoneNumbersDto.getFromPhoneNumber());
		User toUser=userService.findUserByPhoneNumber(fundTransferUsingPhoneNumbersDto.getToPhoneNumber());
		if(fromUser!=null && toUser!=null) {
			UserAccount fromUserAccount=userAccountRepository.findByUser_userId(fromUser.getUserId());
			UserAccount toUserAccount=userAccountRepository.findByUser_userId(toUser.getUserId());
			FundTransferDto fundTransferDto=new FundTransferDto();
			fundTransferDto.setFromAccountNumber(fromUserAccount.getAccountNumber());
			fundTransferDto.setToAccountNumber(toUserAccount.getAccountNumber());
			fundTransferDto.setAmount(fundTransferUsingPhoneNumbersDto.getAmount());
			fundTransferDto.setDescription(fundTransferUsingPhoneNumbersDto.getDescription());
			//fundTransferUsingPhoneNumbersDto.setDate(date);
			fundTransferDto.setDate(LocalDateTime.now());
			UserFundTransfer userFundTransfer=userFundTransferService.addFund(fundTransferDto);
			return "the amount "+fundTransferUsingPhoneNumbersDto.getAmount()+" is transferred from  "+fundTransferUsingPhoneNumbersDto.getFromPhoneNumber()+"to  "+fundTransferUsingPhoneNumbersDto.getToPhoneNumber();
			
		}
		else if(fromUser==null) {
			throw new ResourceNotFoundException(" fromuser not found.Please check");
		}
		else if(toUser==null) {
			throw new ResourceNotFoundException(" touser not found.Please check");
		}
		else {
			throw new ResourceNotFoundException(" users are not found.Please check");
		}
		
		
	}

}
