package com.bankapp.service;

import com.bankapp.dto.FundTransferUsingPhoneNumbersDto;

public interface FundTransferUsingPhoneNumbersService {

	String transferAmount(FundTransferUsingPhoneNumbersDto fundTransferUsingPhoneNumbersDto);

}
