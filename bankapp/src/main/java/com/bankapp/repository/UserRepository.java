package com.bankapp.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.bankapp.model.User;

public interface UserRepository extends JpaRepository<User,Integer> {

	User findByEmail(String email);

	User findByPhoneNumber(String phoneNumber);

}
